Program that gets a family's information as input from a text file and display it.

This a guide on how to format your text file.
First line: must be the root node >> <great..great..grandparents> name (no spaces)
Next line: <great..great..grandparents> name followed by a space then one of their children names.
Next line: repeat the previous step for the number of <great..great..grandparents> children.
Afterward: the line format is the parent name first, followed by space, then the parent's children
You can image it as:

root(parent name)
child parent name followed by the child name.
child parent name followed by the child name.
repeat...

Example:
David has 3 children>> Maria, Dan and Mary
Maria  and Dan have no kids, but Mary has 2 children Anna and Robert
Anna has no children, Robert has Jack.
Here how to write it:

David
David Maria
David Dan
David Mary
Mary Anna
Mary Robert
Robert Jack
