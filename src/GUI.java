import javax.swing.SwingUtilities;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 * A GUI class to representation a family tree.
 * @author D-Abraham
 */
public class GUI extends JPanel {
	private  Tree tree;
	private ArrayList<Person> list;

	/**
	 * Construct a new GUI.
	 * @param fileName file name to build the tree from
	 */
	public GUI(String fileName){
		tree = new Tree();
		tree.readTree(fileName);
		list = toArray();
		assignPoints();
	}

	/**
	 * Assigns x and y points of each node in the tree.
	 */
	private void assignPoints(){
		tree.getRoot().setPoint(600,30);
		Person parent;
		for (int i = 1; i<list.size();i++){
			parent = list.get(i).getParent();
			if(parent.getlChild().equals(list.get(i))){
				list.get(i).setPoint(parent.getPoint().getX()-100,parent.getPoint().getY()+40);
			}
			else if(parent.getmChild().equals(list.get(i))){
				list.get(i).setPoint(parent.getPoint().getX(),parent.getPoint().getY()+50);
			}
			else if (parent.getrChild().equals(list.get(i))){
				list.get(i).setPoint(parent.getPoint().getX()+100,parent.getPoint().getY()+40);
			}
		}
	}

	/**
	 * Returns an array with preorder representation of the tree.
	 * @return ArrayList<Person>
	 */
	private ArrayList<Person> toArray() {
		ArrayList<Person> list = new ArrayList<Person>();
		preOrderTraverse(tree.getRoot(), list);
		return list;
	}

	/**
	 * Perform a preorder traversal.
	 * @param node The local root
	 */
	private void preOrderTraverse(Person node,ArrayList<Person> list) {
		if (node != null) {
			list.add(node);
			preOrderTraverse(node.getlChild(),list);
			preOrderTraverse(node.getmChild(),list);
			preOrderTraverse(node.getrChild(), list);
		}
	}

	@Override
	public void paintComponent(Graphics g){

		for (Person p:list) {
			g.drawString(p.getName(),(int)p.getPoint().getX(),(int)p.getPoint().getY());
			if(p.hasParent()){
				g.drawLine((int)p.getPoint().getX()+20,
						(int)p.getPoint().getY()-15,
						(int)p.getParent().getPoint().getX()+20,
						(int)p.getParent().getPoint().getY()+15);
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(1200, 1000);
	}

	//main method to launch GUI...
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame();
				GUI gui = new GUI("test.txt");
				frame.add(gui);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});

	}
}