import java.awt.Point;

/**
 * This class represents/implements a person, which is used as Tree node.
 * @author D-Abraham
 */
public class Person {
	
	private String name;
	private Person parent;
	private Person lChild;
	private Person mChild;
	private Person rChild;
	private Point point;

	/**
	 * Construct a person with the given name
	 * @param name The person name
	 */
	public Person(String name) {
		this.name = name;
		parent = null;
		lChild = null;
		mChild = null;
		rChild = null;
		point = new Point();
	}

	/**
	 * Construct a person with the given name and link it to parent, lChild, mChild
	 * and rChild nodes. Point set to default values x=0 and y=0.
	 * @param name person name
	 * @param parent parent node
	 * @param lChild left child node
	 * @param rChild right child node
	 * @param mChild middle child node
	 */
	public Person(String name, Person parent, Person lChild, Person rChild,Person mChild) {
		this.name = name;
		this.parent = parent;
		this.lChild = lChild;
		this.mChild = mChild;
		this.rChild = rChild;
		point = new Point(0,0);
	}

	/**
	 * Returns person name
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns parent node
	 * @return parent node
	 */
	public Person getParent() {
		return parent;
	}

	/**
	 * Returns left child of the current node.
	 * @return left child
	 */
	public Person getlChild() {
		return lChild;
	}

	/**
	 * Returns middle child of the current node
	 * @return middle child
	 */
	public Person getmChild() {
		return mChild;
	}

	/**
	 * Returns right child of the current node
	 * @return middle child
	 */
	public Person getrChild() {
		return rChild;
	}

	/**
	 * Assign  current node lChild to the given node
	 * @param lChild node to be assigned as left child
	 */
	public void setlChild(Person lChild) {
		this.lChild = lChild;
	}

	/**
	 * Assign  current node mChild to the given node
	 * @param mChild node to be assigned as middle child
	 */
	public void setmChild(Person mChild) {
		this.mChild = mChild;
	}

	/**
	 * Assign  current node rChild to the given node
	 * @param rChild node to be assigned as right child
	 */
	public void setrChild(Person rChild) {
		this.rChild = rChild;
	}

	/**
	 * Returns point
	 * @return Point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * Changes the point x and y to the given x and y.
	 * @param x the new x value
	 * @param y the new y value
	 */
	public void setPoint(double x, double y){
		point.setLocation(x,y);
	}

	/**
	 * Returns true if person has parent linked to it, otherwise returns false
	 * @return true if person has parent
	 */
	public boolean hasParent() {
		return parent != null;
	}

	/**
	 * Returns true if person has left child linked to it, otherwise returns false
	 * @return true if person has left child
	 */
	public boolean hasLChild() {
		return lChild != null;
	}

	/**
	 * Returns true if person has middle child linked to it, otherwise returns false
	 * @return true if person has middle child
	 */
	public boolean hasMChild() {
		return mChild != null;
	}

	/**
	 * Returns true if person has right child linked to it, otherwise returns false
	 * @return true if person has right child
	 */
	public boolean hasRChild() {
		return rChild != null;
	}

	@Override
	public String toString(){
		return "Name: "+name+", parent(s): "+parent.getName()+", LChild: "+ hasLChild()+
				", MChild: "+ hasMChild()+", RChild:"+ hasRChild();
	}

	@Override
	public boolean equals(Object obj){
		if(obj == this) return true;
		if(obj == null) return false;
		if(this.getClass() == obj.getClass()){
			Person person = (Person) obj;
			return (this.name.matches(person.getName()) && (this.parent == person.getParent()));
		}
		else {
			return false;
		}
	}


}
