import java.util.ArrayList;
//This tester is for the backend only and not for GUI.
public class BackEndTester {
    public static void main(String[] args) {

        Tree tree = new Tree("ggg");
        System.out.println("Root name "+tree.getRoot().getName());
        tree.add("gg","ggg");
        tree.add("gg2","ggg");
        System.out.println(tree.findPerson("gg2").hasParent());
        System.out.println(tree.findPerson("gg").hasParent());
        System.out.println("Find and print gg " +tree.findPerson("gg").getName());
        System.out.println("Find and print gg2 " +tree.findPerson("gg2").getName());
        tree.add("g","gg");
        tree.add("mom","g");
        tree.add("me","mom");
        System.out.println("find me "+ tree.findPerson("me").toString());
        System.out.println(tree.toString());
        tree = new Tree();
        tree.readTree("test.txt");
        System.out.println(tree.toString());

    }
}
