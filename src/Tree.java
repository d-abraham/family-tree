import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class is an implementation of Tree, each node has three children(left, middle
 * and right child) and reference to parent node.
 * Please read README for more details on how to format the text file.
 * @author D-Abraham
 */

public class Tree {
    //root node
    private Person root;

    /**
     * Construct a new tree with null root
     */
    public Tree(){
        root = null;
    }

    /**
     * Construct a new tree with new root/Person
     * @param name name of the root person
     */
    public Tree(String name){
        root = new Person(name);
    }

    /**
     * Adds a new node/person to the tree.
     * First it finds the parent node, then add the new node to it.
     * If the left child is null, it will assign the new person to it.
     * If left child is not null, then it will try middle child, if not, then right
     * child.
     * @param newPersonName new person name
     * @param parentName parent of the new person
     */
    public void add(String newPersonName, String parentName){
        Person tmp = findPerson(parentName);
        if(tmp!=null){
            if(!tmp.hasLChild()){
                tmp.setlChild(new Person(newPersonName,tmp,null,null,null));
            }
            else if (!tmp.hasMChild()){
                tmp.setmChild(new Person(newPersonName,tmp, null,null,null));
            }
            else if(!tmp.hasRChild()){
                tmp.setrChild(new Person(newPersonName,tmp, null,null,null));
            }
        }
    }

    /**
     * Recursively finds a person with the given name, otherwise, returns null.
     * @param name name of the person
     * @return returns the person node or null if not found
     */
    public Person findPerson(String name){
        return findPerson(name,root);
    }
    private Person findPerson(String name, Person root){
        if(root != null) {
            if (root.getName().equals(name)) {
                return root;
            } else {
                Person person = findPerson(name, root.getlChild());
                if (person == null) {
                    person = findPerson(name, root.getrChild());
                }
                if (person ==null){
                    person = findPerson(name,root.getmChild());
                }
                return person;
            }
        }
        else {
            return null;
        }
    }

    /**
     * Returns the root node
     * @return root
     */
    public Person getRoot(){
        return root;
    }

    /**
     * Perform a preorder traversal
     * @return Preorder representation of the tree
     */
    public String preOrderTraverse() {
        StringBuilder sb = new StringBuilder();
        preOrderTraverse(root,1, sb);
        return sb.toString();
    }
    /**
     * Perform a preorder traversal.
     * @param node The local root
     * @param depth The depth
     * @param sb The string buffer to save the output
     */
    private void preOrderTraverse(Person node, int depth, StringBuilder sb) {
        for (int i = 1; i < depth; i++) {
            sb.append("  ");
        }
        if (node == null) {
            sb.append("null\n");
        } else {
            sb.append(node.getName());
            sb.append("\n");
            preOrderTraverse(node.getlChild(), depth + 1, sb);
            preOrderTraverse(node.getmChild(), depth + 1, sb);
            preOrderTraverse(node.getrChild(), depth + 1, sb);
        }
    }

    /**
     * Builds a tree from an external text file. Please read README for more details
     * on how to format the text file.
     * @param fileName name of the file
     * @throws Exception If there is IO error or parent of a node not found
     */
    public void readTree(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            root = new Person(line);
            line = reader.readLine();
            Person parent;
            while (line != null && !line.isEmpty()) {
                String[] lineSplit = line.split(" ");
                parent = findPerson(lineSplit[0]);
                if(parent != null){
                    for (int i=1; i<lineSplit.length;i++){
                        add(lineSplit[i],lineSplit[0]);
                    }
                }
                else{
                    throw new Exception("Parent not found");
                }
                line = reader.readLine();
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        preOrderTraverse(root, 1, sb);
        return sb.toString();
    }
}
